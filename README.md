# ขั้นตอนการติดตั้ง MySQL 5.6.45 ผ่าน Docker

การติดตั้ง MySQL เผื่อให้สำหรับเก็บข้อมูลสำหรับโปรแกรม JHCIS

## Download MySQL

Download docker-compose.yml และ data file ผ่าน GitLab

```bash
git clone https://gitlab.com/7.sada/mysql.git
cd mysql
```

## พิมพ์คำสั่ง 

```
docker-compose up -d
```
